package com.taylorbockman.na

import scala.annotation.tailrec

/* RootFinder
 *
 * (c) 2014 Taylor Bockman
 */
class RootFinder{
  
  /*
   * Runs the bisection method for root finding.
   * 
   * For tolerances it uses |Pn - Pn-1|/|p| (should this be used always, can p = 0?)
   * since it seems to be the best at approximating the relative error.
   *
   * Poly: A function representing the function to find roots for.
   *       ex: (x: Double) => x - Math.exp(x)
   *
   * n: Maximum number of iterations to attempt to approximate with (default = 10000)
   * 
   * tol: The tolerance level to approximate to (default = 10^-4)
   *   
   * interval: The interval pair in the form (a, b) representing [a, b]
   *
   * returns a tuple (i, p, e) where i is the root number (such as p14), p is the 
   * approximated root, and e is the error at the last iteration. Note, it will always
   * return something, either if the tolerance is reach or N goes to 0. Pay attention to
   * the output error and Pn number.
   *
   * Note: |Pn - P| <= (b-a)/(2^n)     (|Pn - P| error bound)
   * 
   * TODO:
   *  If the interval contains zero switch to absolute error since it is possible
   *  that Pn will eventually be zero and break the error calculation.
   *
   */
  def bisectionMethod(poly: Double  => Double, 
                      interval: (Double, Double), 
                      tol: Double = 0.0001,
                      n: Int = 10000): Option[(Int, Double, Double)] = {
    
    //Preserve Intermediate Value Theorem. We can only have a zero between a and b
    //if f(a) and f(b) have differing signs.
    if(poly(interval._1) * poly(interval._2) > 0) return None

    @tailrec
    def calculate(poly: Double  => Double, a: Double, b: Double, pPrev: Double, 
                  pCount: Int, prevError: Double, 
                  tol: Double, n: Int): Option[(Int, Double, Double)] = {
        
      if(n == 0) return None
      
      //Note: a + (b-a)/2 is used over (a+b)/2 to avoid overflow
      val p: Double = a + (b-a)/2
      val fp: Double = poly(p)
      val fa: Double = poly(a)
      val error: Double = Math.abs((p - pPrev))/Math.abs(p)

      //Success
      if(fp == 0 || error < tol) return Some((pCount, p, error))
      
      //If same sign let a = p and run the interval from [p, b]
      //If not same sign, let b = p and run the interval from [a, p]
      //Note, p will be between a and b.
      if(fa * fp > 0) calculate(poly, p, b, p, pCount + 1, error, tol, n - 1)
      else calculate(poly, a, p, p, pCount + 1, error, tol, n -1)


    }
    
    
    calculate(poly, interval._1, interval._2, 0, 1, 0, tol, n)

  }
  
  //Method of fixed points. The user is responsible for confirming the two 
  //requirements. Documentation not done, uses absolute error.
  //
  // (i) g exists on C[a,b] and g(x) maps into [a,b]
  // (ii) |g'(x)| < k <= 1 for all x in [a,b]
  def fixedPoint(g: Double => Double, p0: Double,
                 tol: Double = 0.0001, 
                 n: Int = 10000): Option[(Int, Double, Double)] = {
 
    def calculate(g: Double => Double, p0: Double,
                  tol: Double = 0.0001, pCount: Int,
                  n: Int): Option[(Int, Double, Double)] = {

      if(n == 0) return None

      val p: Double = g(p0)
      val error: Double = Math.abs(p - p0)
      
      if(error < tol) return Some((pCount, p, error))

      calculate(g, p, tol, pCount + 1, n - 1)
    }

    calculate(g, p0, tol, 1, n)
  
  }

  /*
   * More docs needed
   * Note n = n -1 here. So make n 1 less than what you want.
   *      This is done to match the book. It shouldn't matter in practice.
   *
   * Weird thing, it appears to be returning 1 less p than what the manual
   * solution gives, except the answer and error are both spot on.
   */
  def newtonsMethod(fx: Double => Double, fpx: Double => Double, 
                    p0: Double, tol: Double = 0.0001,
                    n: Int = 10000): Option[(Int, Double, Double)] = {

    @tailrec
    def calculate(fx: Double => Double, fpx: Double => Double,
                  pPrev: Double, prevError: Double, pCount: Int, 
                  tol: Double, n: Int): Option[(Int, Double, Double)] = {

      if(n == 0) return None
      
      //p = p0 - f(p0)/f'(p0)
      val p: Double = pPrev - (fx(pPrev)/fpx(pPrev))
      val error: Double = Math.abs(p - pPrev)

     

      if(error < tol) return Some((pCount, p, error))

      calculate(fx, fpx, p, error, pCount + 1, tol, n-1)

     }             
    
    calculate(fx, fpx, p0, 0, 1, tol, n) 
  }


  //Secant method - description to be written. Uses absolute error.
  def secantMethod(fx: Double => Double, p0: Double, p1: Double,
                   tol: Double = 0.0001,
                   n: Int = 10000): Option[(Int, Double, Double)] = {
  
    @tailrec
    def calculate(fx: Double => Double, p0: Double, p1: Double, 
                  q0: Double, q1: Double, pCount: Int, tol: Double,
                  n: Int): Option[(Int, Double, Double)] = {
      
      if(n == 0) return None

      val p: Double = p1 - (q1 * (p1 - p0)/(q1-q0))
      val error: Double = Math.abs(p - p1)

      if(error < tol) return Some((pCount, p, error))

      calculate(fx, p1, p, q1, fx(p), pCount + 1, tol, n - 1)
       
    }

    return calculate(fx, p0, p1, fx(p0), fx(p1), 2, tol, n)
  }

  //Method of false position - description to be written uses absolute error.
  //Note - false position is basically the secant method, but with a check
  //similar to bisection method to insure that the algorithm is bracketing a root
  def falsePosition(fx: Double => Double, p0: Double, p1: Double,
                    tol: Double = 0.0001,
                    n: Int = 10000): Option[(Int, Double, Double)] = {

     //Initial condition isn't met. [p0, p1] does not bracket a root.
     if(fx(p0) * fx(p1) > 0) return None
     
     @tailrec
     def calculate(fx: Double => Double, p0: Double, p1: Double,
                   q0: Double, q1: Double, pCount: Int, tol: Double,
                   n: Int): Option[(Int, Double, Double)] = {
     
        //It failed to get within the tolerance after n iterations
        if(n == 0) return None

        val p: Double = p1 - (q1 * (p1 - p0)/(q1 - q0))
        val error: Double = Math.abs(p - p1)

        if(error < tol) return Some((pCount, p, error))

        val q: Double = fx(p)
        
        //If f(pn)* f(pn-1) < 0, we are bracketing a root
        //so then the next run we want (pn-1, f(pn-1)) to (pn, f(pn))
        //Otherwise we want (pn-2, f(pn-2)) to (pn, f(pn))
        if(q * q1 < 0) calculate(fx, p1, p, q1, q, pCount + 1, tol, n-1)
        else calculate(fx, p0, p, q0, q, pCount + 1, tol, n-1)
     }

     calculate(fx, p0, p1, fx(p0), fx(p1), 2, tol, n) 
  }

  /* Modified newtons method is a way to handle cases where the zero of
   * f(x) is not simple. That is, f(p) = 0 and f'(p) = 0. Newtons method
   * has difficulty converging in the case the zero is not simple.
   */
  def modifiedNewtons(fx: Double => Double, fpx: Double => Double, 
                      fdpx: Double => Double, p0: Double, tol: Double = 0.0001,
                      n: Int = 10000): Option[(Int, Double, Double)] = {

    @tailrec
    def calculate(fx: Double => Double, fpx: Double => Double,
                  fdpx: Double => Double,pPrev: Double, prevError: Double, 
                  pCount: Int, tol: Double, n: Int): Option[(Int, Double, Double)] = {

      if(n == 0) return None
      
      //pn = pn-1 - (f(pn-1)f'(pn-1))/(f'(pn-1)^2 - f(pn-1)f''(pn-1)
      val p: Double = pPrev - (fx(pPrev)*fpx(pPrev))/
                              (Math.pow(fpx(pPrev),2)-(fx(pPrev)*fdpx(pPrev)))
      val error: Double = Math.abs(p - pPrev)
     

      if(error < tol) return Some((pCount, p, error))

      calculate(fx, fpx, fdpx, p, error, pCount + 1, tol, n-1)

     }             
    
    calculate(fx, fpx, fdpx, p0, 0, 1, tol, n) 
  }

  
}
