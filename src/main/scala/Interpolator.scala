/**
 * This class contains all interpolation methods covered by
 * Numerical Analysis 9th Ed.
 *
 * (c) 2014 Taylor Bockman
 */

package com.taylorbockman.na

import scala.annotation.tailrec
import scala.collection.immutable.Vector
import scala.collection.immutable.List
import Array._


/* NOTES:

   This is currently not functional. By replacing Array with an immutable vector and then on all inner/outer calls 
   having inner return the vector result into a val temporary before passing to outer, this will be truly fully
   functional.
   
   Fix when you have time */
   

class Interpolator {

  /*
    Neville's Method is a dynamic programming solution to the polynomial interpolation
    problem. We can save ourselves a lot of work by remembering what we calculated
    previously. Neville's Method takes advantage of this.

    xs: Vector of doubles representing the x's, x_0, x_1,...x_n
    qs: Vector of doubles representing the f(x)'s, f(x_0), f(x_1),...

    Neville's Method returns a list of doubles representing the calculated Q's with P(x) = Q_n,n being the
    last solution.

    In other words, the returned list contains the first, second, third, all the way to nth approximations
    of the polynomial given the data, evaluated at the point x.
   */
  def nevillesMethod(xs:Vector[Double], qs:Vector[Double], x: Double): Option[List[Double]] = {


    val q = Array.ofDim[Double](xs.size, xs.size)

    //Init q
    for(i <- 0 until xs.size) q(i)(0) = qs(i)

    @tailrec
    def outer(q: Array[Array[Double]], i: Int): Array[Array[Double]] = {
      //i goes from 0 to n-1, n is the size of xs.

      if(i == xs.size){
        return q
      }
      inner(q, 1, i)
      outer(q, i+1)
    }

    @tailrec
    def inner(q:Array[Array[Double]], j: Int, i: Int): Array[Array[Double]] = {

      if(j > i) return q
      q(i)(j) = (((x - xs(i-j))*q(i)(j-1)) - ((x - xs(i))*q(i-1)(j-1)))/(xs(i)-xs(i-j))

      inner(q, j+1, i)
    }

    def getDiagonal(q: Array[Array[Double]], size: Int): List[Double] = {

        @tailrec
        def inner(q: Array[Array[Double]], i: Int, l: List[Double]): List[Double] = {

          if(i == size) return l

          inner(q, i+1, l :+ q(i)(i))

        }

        inner(q, 1, List[Double]())

    }

    val outerRes: Array[Array[Double]] = outer(q, 1)

    //The polynomial approximations will be on the diagonal
    //That is the first polynomial approximation will be 1,1, next will be 2,2, next will be 3,3,...
    Some(getDiagonal(outerRes, xs.size))
  }

  /*
    divideDifference runs Newton's Divided Difference on the arguments to produce
    a list of coefficients of the interpolatory polynomial P.

    xs: The vector of x values used - x_0, x_1, ..., x_n
    fs: The vector of y values f(x_0), f(x_1), ..., f(x_n)

    returns: a list of the divided difference coefficients of the interpolatory polynomial P.
   */
  def dividedDifference(xs:Vector[Double], fs:Vector[Double]): Option[List[Double]] = {

    val f = Array.ofDim[Double](xs.size, xs.size)

    //Init F
    for(i <- 0 until xs.size) f(i)(0) = fs(i)

    @tailrec
    def outer(f: Array[Array[Double]], i: Int): Array[Array[Double]] = {
      //i goes from 0 to n-1, n is the size of xs.

      if(i == xs.size){
        return f
      }
      inner(f, 1, i)
      outer(f, i+1)
    }

    @tailrec
    def inner(f:Array[Array[Double]], j: Int, i: Int): Array[Array[Double]] = {

      if(j > i) return f
      f(i)(j) = (f(i)(j-1)-f(i-1)(j-1))/(xs(i)-xs(i-j))

      inner(f, j+1, i)
    }

    def getDiagonal(q: Array[Array[Double]], size: Int): List[Double] = {

      @tailrec
      def inner(f: Array[Array[Double]], i: Int, l: List[Double]): List[Double] = {

        if(i == size) return l

        inner(f, i+1, l :+ f(i)(i))

      }

      inner(f, 0, List[Double]())

    }

    val outerRes: Array[Array[Double]] = outer(f, 1)


    //The divided difference coefficients will be on the diagonal
    //That is the first coefficient will be 1,1, next will be 2,2, next will be 3,3,...
    Some(getDiagonal(outerRes, xs.size))
  }
}
